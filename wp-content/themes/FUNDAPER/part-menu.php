<!-- Begin Menu -->
	<section class="menu_wrap wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<div class="moduletable_m1">
					<div class="top-bar">
						<div class="top-bar-title">
							<span data-responsive-toggle="responsive-menu" data-hide-for="medium">
								<button class="menu-icon" type="button" data-toggle></button>
								<strong>Menú</strong>
							</span>
						</div>
						<div id="responsive-menu">
							<div class="top-bar-left">
								<?php
								wp_nav_menu(
									array(
										'menu_class' => 'dropdown menu',
										'container' => false,
										'theme_location' => 'main-menu',
										'items_wrap' => '<ul class="%2$s" data-dropdown-menu>%3$s</ul>'
									)
								);
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<!-- End Menu -->