<!-- Begin Header -->
	<section class="header wow fadeIn" data-wow-delay="0.5s">
		<div class="row align-middle">
			<div class="small-12 medium-6 columns">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
			<div class="small-12 medium-6 columns align-right">
				<?php dynamic_sidebar( 'social_media' ); ?>
			</div>
		</div>
	</section>
<!-- End Header -->
<?php get_template_part( 'part', 'menu' ); ?>