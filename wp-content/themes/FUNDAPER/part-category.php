<!-- Begin Category -->
	<section class="content wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<div class="box_2">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<h2><?php the_title(); ?></h2>
						<?php the_excerpt(); ?>
						<p class="text-right"><a href="<?php the_permalink(); ?>" class="hollow button warning">Leer más...</a></p>
					<?php endwhile; endif; ?>
				</div>
			</div>
		</div>
	</section>
<!-- End Category -->