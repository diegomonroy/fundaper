<!-- Begin Help -->
	<section class="help wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'help' ); ?>
			</div>
		</div>
	</section>
<!-- End Help -->