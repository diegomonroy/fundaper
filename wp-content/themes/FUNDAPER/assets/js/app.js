// JavaScript Document

/* ************************************************************************************************************************

FUNDAPER

File:			app.js
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2017

************************************************************************************************************************ */

/* Foundation */

$(document).foundation();

/* WOW */

new WOW().init();

/* jQuery */

jQuery.noConflict();

jQuery(document).ready(function () {
	jQuery( 'ul.sub-menu' ).addClass( 'menu' );
});